package com.techasiaportal.cc.solution.adrian.commons.persistence.api.dao.jpa;

import cz.jirutka.rsql.parser.ast.*;
import org.springframework.data.jpa.domain.Specification;

public class DefaultRsqlVisitor<T> extends NoArgRSQLVisitorAdapter<Specification<T>> {

    private final GenericRsqlSpecBuilder<T> builder;

    public DefaultRsqlVisitor() {
        this.builder = new GenericRsqlSpecBuilder<>();
    }

    @Override
    public Specification<T> visit(AndNode andNode) {
        return builder.createSpecification(andNode);
    }

    @Override
    public Specification<T> visit(OrNode orNode) {
        return builder.createSpecification(orNode);
    }

    @Override
    public Specification<T> visit(ComparisonNode comparisonNode) {
        return builder.createSpecification(comparisonNode);
    }
}
