package com.techasiaportal.cc.solution.adrian.commons.persistence.implementation.infra;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.infra.MapperFactoryAware;
import ma.glasnost.orika.MapperFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MapperFactoryAwareBeanPostProcessor implements BeanPostProcessor {

    private final MapperFactory mapperFactory;

    public MapperFactoryAwareBeanPostProcessor(MapperFactory mapperFactory) {
        this.mapperFactory = mapperFactory;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if(bean instanceof MapperFactoryAware){
            MapperFactoryAware mapperFactoryAware = (MapperFactoryAware) bean;
            mapperFactoryAware.setMapperFactory(mapperFactory);
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }
}
