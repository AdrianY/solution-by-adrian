package com.techasiaportal.cc.solution.adrian.commons.persistence.api.services;

import java.util.List;

public interface MapperService {
    <TARGET, SOURCE> List<TARGET> map(List<SOURCE> sourceList, Class<TARGET> targetClass);

    <TARGET, SOURCE> TARGET map(SOURCE source, Class<TARGET> targetClass);
}
