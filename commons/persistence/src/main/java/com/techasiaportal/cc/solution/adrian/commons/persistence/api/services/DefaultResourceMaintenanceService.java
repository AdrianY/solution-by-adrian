package com.techasiaportal.cc.solution.adrian.commons.persistence.api.services;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.dao.jpa.BaseJpaDao;
import com.techasiaportal.cc.solution.adrian.commons.persistence.api.dao.jpa.DefaultRsqlVisitor;
import com.techasiaportal.cc.solution.adrian.commons.persistence.api.entities.BaseEntity;
import com.techasiaportal.cc.solution.adrian.commons.persistence.api.exceptions.ResourceNotFoundException;
import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.ast.Node;
import cz.jirutka.rsql.parser.ast.RSQLVisitor;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.Assert;

import javax.validation.Valid;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class DefaultResourceMaintenanceService<DTO, ENTITY extends BaseEntity>
        implements ResourceMaintenanceService<DTO>, MapperServiceAware, InitializingBean {

    private Class<ENTITY> entityType;
    private Class<DTO> dtoType;

    private BaseJpaDao<ENTITY> dao;
    private RSQLVisitor<Specification<ENTITY>, Void> rsqlVisitor;

    private MapperService mapperService;

    public void setDao(BaseJpaDao<ENTITY> dao) {
        this.dao = dao;
    }

    @Override
    public void setMapperService(MapperService mapperService) {
        this.mapperService = mapperService;
    }

    protected MapperService getMapper(){
        return this.mapperService;
    }

    @SuppressWarnings("unchecked")
    private Class<ENTITY> getEntityType() {
        return (Class<ENTITY>) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[1];
    }

    @SuppressWarnings("unchecked")
    private Class<DTO> getResourceType() {
        return (Class<DTO>) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
    }

    @Override
    public List<DTO> findAll(String searchString) {


        if (StringUtils.isNotEmpty(searchString)) {
                        Specification<ENTITY> spec = createRsqlSpec(searchString);
            Iterable<ENTITY> entities = dao.findAll(spec);
            return mapperService.map(IterableUtils.toList(entities), dtoType);
        } else
            return findAll();

    }

    @Override
    public List<DTO> findAll() {
        return mapperService.map(IterableUtils.toList(dao.findAll()), dtoType);
    }

    @Override
    public DTO create(DTO resource) {
        ENTITY entity = mapperService.map(resource, entityType);
        return mapperService.map(dao.save(entity), dtoType);
    }

    @Override
    public void remove(Long resourceId) {
        if(!dao.exists(resourceId))
            throw new ResourceNotFoundException(resourceId);
        dao.delete(resourceId);
    }

    @Override
    public void afterPropertiesSet() {
        Assert.notNull(this.dao, "dao must be set");
        Assert.notNull(this.mapperService, "mapper service must have been set");
        this.entityType = getEntityType();
        this.dtoType = getResourceType();
        this.rsqlVisitor = new DefaultRsqlVisitor<>();
    }

    private Specification<ENTITY> createRsqlSpec(String searchString){
        Node rootNode = new RSQLParser().parse(searchString);
        return rootNode.accept(rsqlVisitor);
    }

}
