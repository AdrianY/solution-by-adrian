package com.techasiaportal.cc.solution.adrian.commons.persistence;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.services.MapperService;
import com.techasiaportal.cc.solution.adrian.commons.persistence.implementation.infra.MapperFactoryAwareBeanPostProcessor;
import com.techasiaportal.cc.solution.adrian.commons.persistence.implementation.services.MapperServiceAwareBeanPostProcessor;
import com.techasiaportal.cc.solution.adrian.commons.persistence.implementation.services.OrikaBasedMapperService;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainPersistenceConfig {

    @Bean
    public MapperFactory mapperFactory(){
        return new DefaultMapperFactory.Builder().build();
    }

    @Bean
    public MapperService mapperService(MapperFactory mapperFactory){
        return new OrikaBasedMapperService(mapperFactory);
    }

    @Bean
    public BeanPostProcessor mapperServiceAwareBeanPostProcessor(MapperService mapperService){
        return new MapperServiceAwareBeanPostProcessor(mapperService);
    }

    @Bean
    public BeanPostProcessor mapperFactoryAwareBeanPostProcessor(MapperFactory mapperFactory){
        return new MapperFactoryAwareBeanPostProcessor(mapperFactory);
    }
}
