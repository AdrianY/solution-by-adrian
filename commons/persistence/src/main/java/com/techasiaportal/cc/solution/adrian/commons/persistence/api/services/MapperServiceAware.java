package com.techasiaportal.cc.solution.adrian.commons.persistence.api.services;

public interface MapperServiceAware {
    void setMapperService(MapperService mapperService);
}
