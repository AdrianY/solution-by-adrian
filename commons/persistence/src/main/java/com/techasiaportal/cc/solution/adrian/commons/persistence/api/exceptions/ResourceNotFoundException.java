package com.techasiaportal.cc.solution.adrian.commons.persistence.api.exceptions;

public class ResourceNotFoundException extends RuntimeException{
    private final Object identifier;

    public ResourceNotFoundException(Object identifier) {
        super();
        this.identifier = identifier;
    }

    public Object getIdentifier() {
        return identifier;
    }
}
