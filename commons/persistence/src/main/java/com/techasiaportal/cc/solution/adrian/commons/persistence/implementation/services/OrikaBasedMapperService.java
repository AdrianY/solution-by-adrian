package com.techasiaportal.cc.solution.adrian.commons.persistence.implementation.services;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.services.MapperService;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class OrikaBasedMapperService implements MapperService {

    private final MapperFacade mapperFacade;

    public OrikaBasedMapperService(MapperFactory mapperFactory) {
        this.mapperFacade = mapperFactory.getMapperFacade();
    }

    @Override
    public <TARGET, SOURCE> List<TARGET> map(List<SOURCE> sources, Class<TARGET> targetClass) {
        if(CollectionUtils.isEmpty(sources))
            return Collections.emptyList();

        return sources.stream()
                .map(source -> mapperFacade.map(source, targetClass))
                .collect(Collectors.toList());
    }

    @Override
    public <TARGET, SOURCE> TARGET map(SOURCE source, Class<TARGET> targetClass) {
        return mapperFacade.map(source, targetClass);
    }
}
