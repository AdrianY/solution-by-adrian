package com.techasiaportal.cc.solution.adrian.commons.persistence.api.services;

import javax.validation.Valid;
import java.util.List;

public interface ResourceMaintenanceService<T> {
    List<T> findAll(String searchString);

    List<T> findAll();

    T create(@Valid T resource);

    void remove(Long resourceId);
}
