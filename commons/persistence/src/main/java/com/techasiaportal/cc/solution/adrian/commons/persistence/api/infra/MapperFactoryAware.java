package com.techasiaportal.cc.solution.adrian.commons.persistence.api.infra;

import ma.glasnost.orika.MapperFactory;

public interface MapperFactoryAware {
    void setMapperFactory(MapperFactory mapperFactory);
}
