package com.techasiaportal.cc.solution.adrian.commons.persistence.api.dto;

public abstract class BaseDTO {
    private Long id;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
