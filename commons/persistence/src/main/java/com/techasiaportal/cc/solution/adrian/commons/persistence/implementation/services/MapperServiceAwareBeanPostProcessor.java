package com.techasiaportal.cc.solution.adrian.commons.persistence.implementation.services;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.services.MapperService;
import com.techasiaportal.cc.solution.adrian.commons.persistence.api.services.MapperServiceAware;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MapperServiceAwareBeanPostProcessor implements BeanPostProcessor {

    private final MapperService mapperService;

    public MapperServiceAwareBeanPostProcessor(MapperService mapperService) {
        this.mapperService = mapperService;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if(bean instanceof MapperServiceAware){
            MapperServiceAware mapperServiceAware = (MapperServiceAware) bean;
            mapperServiceAware.setMapperService(mapperService);
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }
}
