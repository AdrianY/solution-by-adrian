package com.techasiaportal.cc.solution.adrian.commons.persistence.api.dao.jpa;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.entities.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface BaseJpaDao<T extends BaseEntity> extends JpaRepository<T, Long>, JpaSpecificationExecutor<T> {
}
