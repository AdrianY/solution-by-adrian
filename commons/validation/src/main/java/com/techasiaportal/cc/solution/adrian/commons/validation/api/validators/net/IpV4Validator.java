package com.techasiaportal.cc.solution.adrian.commons.validation.api.validators.net;

import com.techasiaportal.cc.solution.adrian.commons.validation.api.validators.NonNullConstraintValidator;
import org.apache.commons.validator.routines.InetAddressValidator;

public class IpV4Validator extends NonNullConstraintValidator<IpV4, String> {

    @Override
    protected boolean doValidation(String value, IpV4 constraintAnnotation) {
        return InetAddressValidator.getInstance().isValidInet4Address(value);
    }

    @Override
    protected String getValueName(IpV4 constraintAnnotation) {
        return constraintAnnotation.valueName();
    }

    @Override
    protected String customErrorMessage(IpV4 constraintAnnotation) {
        return constraintAnnotation.message();
    }
}
