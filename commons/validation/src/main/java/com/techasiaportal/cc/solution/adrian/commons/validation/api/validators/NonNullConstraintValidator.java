package com.techasiaportal.cc.solution.adrian.commons.validation.api.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;
import java.util.Objects;

public abstract class NonNullConstraintValidator<C extends Annotation, T> implements ConstraintValidator<C, T> {

    private String valueName;
    private C constraintAnnotation;

    @Override
    public void initialize(C constraintAnnotation) {
        valueName = getValueName(constraintAnnotation);
        this.constraintAnnotation = constraintAnnotation;
    }

    @Override
    public boolean isValid(T value, ConstraintValidatorContext context) {
        if(Objects.isNull(value)){
            context.buildConstraintViolationWithTemplate(valueName + " cannot be blank")
                    .addConstraintViolation()
                    .disableDefaultConstraintViolation();

            return false;
        } else {
            boolean valid = doValidation(value, constraintAnnotation);
            if(!valid){
                String messageTemplate = customErrorMessage(constraintAnnotation);
                context.buildConstraintViolationWithTemplate(messageTemplate)
                        .addConstraintViolation()
                        .disableDefaultConstraintViolation();
            }
            return valid;
        }
    }

    protected abstract boolean doValidation(T value, C constraintAnnotation);

    protected abstract String getValueName(C constraintAnnotation);

    protected abstract String customErrorMessage(C constraintAnnotation);
}
