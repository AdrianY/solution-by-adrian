package com.techasiaportal.cc.solution.adrian.commons.validation.api.validators.net;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = IpV4Validator.class)
public @interface IpV4 {
    String valueName() default "IP";
    String message() default "Invalid IPv4 IP Address";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
