package com.techasiaportal.cc.solution.adrian.commons.rest.infra.documentation;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.techasiaportal.cc.solution.adrian.commons.rest.infra.documentation.RestConfigReferences.API_DOCUMENTATION_PROPERTIES_PREFIX;
import static springfox.documentation.builders.PathSelectors.regex;


@Configuration
@EnableSwagger2
@EnableConfigurationProperties
public class DocumentationConfig {

    @Bean
    @ConfigurationProperties(API_DOCUMENTATION_PROPERTIES_PREFIX)
    public ApiDocDetails apiDocDetails(){
        return new ApiDocDetails();
    }

    @SuppressWarnings("Guava")
    @Bean
    public Docket api(ApiDocDetails apiDocDetails) {

        ApiInfo apiInfo = deriveApiInfo(apiDocDetails);

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(apiDocDetails.getVersion())
                .apiInfo(apiInfo)
                .securityContexts(Collections.singletonList(securityContext(apiDocDetails)))
                .securitySchemes(Collections.singletonList(apiKey()))
                .select()
                .paths(Predicates.not(PathSelectors.regex("/error")))
                .build()
                .useDefaultResponseMessages(false)
                .globalResponseMessage(
                        RequestMethod.DELETE,
                        Arrays.asList(
                                getForbiddenResponse(),
                                getInvalidCredentialsResponse(),
                                getMimeTypeNotSupportedResponse()
                        )
                )
                .globalResponseMessage(
                        RequestMethod.GET,
                        Arrays.asList(
                                getForbiddenResponse(),
                                getInvalidCredentialsResponse(),
                                getMimeTypeNotSupportedResponse()
                        )
                )
                .globalResponseMessage(
                        RequestMethod.POST,
                        Arrays.asList(
                                getForbiddenResponse(),
                                getInvalidCredentialsResponse(),
                                getMimeTypeNotSupportedResponse(),
                                getBodyNotParseableResponse()
                        )
                );
    }

    @SuppressWarnings("Guava")
    private Predicate<String> getPath(ApiDocDetails apiDocDetails) {
        return regex(".\\*/" + apiDocDetails.getVersion() + ".\\*");
    }

    @Bean
    public SecurityConfiguration security() {
        return new SecurityConfiguration(
                null,
                null,
                null,
                null,
                "Bearer ",
                ApiKeyVehicle.HEADER,
                "Authorization",
                null
        );
    }

    private ApiKey apiKey() {
        return new ApiKey("Authorization", "Authorization", "header");
    }

    private SecurityContext securityContext(ApiDocDetails apiDocDetails) {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(getPath(apiDocDetails))
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope
                = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Collections.singletonList(
                new SecurityReference("Authorization", authorizationScopes));
    }

    private static ApiInfo deriveApiInfo(ApiDocDetails apiDocDetails) {

        ApiDocDetails.ContactDetails contactDetails = apiDocDetails.getContactDetails();

        return new ApiInfoBuilder()
                .contact(new Contact(
                        contactDetails.getName(),
                        contactDetails.getUrl(),
                        contactDetails.getEmail()
                ))
                .title(apiDocDetails.getTitle())
                .description(apiDocDetails.getDescription())
                .license(apiDocDetails.getLicense())
                .licenseUrl(apiDocDetails.getLicenseUrl())
                .version(apiDocDetails.getVersion())
                .build();
    }

    private static ResponseMessage getBodyNotParseableResponse() {
        return new ResponseMessageBuilder()
                .code(400)
                .message("Cannot parse request. Make sure request body format is aligned with the Content-Type")
                .responseModel(new ModelRef("RestApiErrors"))
                .build();
    }

    private static ResponseMessage getMimeTypeNotSupportedResponse() {
        return new ResponseMessageBuilder()
                .code(415)
                .message("Media type is not supported")
                .responseModel(new ModelRef("RestApiErrors"))
                .build();
    }

    private static ResponseMessage getForbiddenResponse() {
        return new ResponseMessageBuilder()
                .code(403)
                .message("Access Denied")
                .build();
    }

    private static ResponseMessage getInvalidCredentialsResponse() {
        return new ResponseMessageBuilder()
                .code(401)
                .message("Authentication Failed: Bad credentials")
                .build();
    }

}
