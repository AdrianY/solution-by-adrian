package com.techasiaportal.cc.solution.adrian.commons.rest.implementation.handler;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.exceptions.ResourceNotFoundException;
import com.techasiaportal.cc.solution.adrian.commons.rest.api.support.RestApiErrors;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;


@ControllerAdvice
public class DefaultResourceExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
            HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String errorMessage = "Cannot parse request. Make sure request body format is aligned with the Content-Type: " +
                request.getHeader("Content-Type");
        RestApiErrors restApiErrors = new RestApiErrors(
                HttpStatus.BAD_REQUEST,
                ex.getLocalizedMessage(),
                errorMessage
        );
        return new ResponseEntity<>(restApiErrors, new HttpHeaders(), restApiErrors.getStatus());
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
            HttpMediaTypeNotSupportedException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request
    ) {
        StringBuilder builder = new StringBuilder();
        builder.append(ex.getContentType());
        builder.append(" media type is not supported. Supported media types are ");
        ex.getSupportedMediaTypes().forEach(t -> builder.append(t).append(", "));

        RestApiErrors restApiErrors = new RestApiErrors(HttpStatus.UNSUPPORTED_MEDIA_TYPE,
                ex.getLocalizedMessage(), builder.substring(0, builder.length() - 2));
        return new ResponseEntity<>(restApiErrors, new HttpHeaders(), restApiErrors.getStatus());
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(
            NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String error = "No handler found for " + ex.getHttpMethod() + " " + ex.getRequestURL();

        RestApiErrors restApiError = new RestApiErrors(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(restApiError, new HttpHeaders(), restApiError.getStatus());
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            HttpRequestMethodNotSupportedException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        StringBuilder builder = new StringBuilder();
        builder.append(ex.getMethod());
        builder.append(" method is not supported for this request. Supported methods are ");
        ex.getSupportedHttpMethods().forEach(t -> builder.append(t).append(" "));

        RestApiErrors restApiErrors = new RestApiErrors(HttpStatus.METHOD_NOT_ALLOWED,
                ex.getLocalizedMessage(), builder.toString());
        return new ResponseEntity<>(restApiErrors, new HttpHeaders(), restApiErrors.getStatus());
    }

    @ExceptionHandler(value = ResourceNotFoundException.class)
    protected ResponseEntity<Object> handleNotFoundError(ResourceNotFoundException ex, WebRequest webRequest){
        RestApiErrors restApiErrors = new RestApiErrors(
                HttpStatus.NOT_FOUND,
                "Error retrieving resource record",
                String.format(
                        "Record for specified resource with id %s is not found", ex.getIdentifier())
        );
        return new ResponseEntity<>(restApiErrors, new HttpHeaders(), restApiErrors.getStatus());
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConflict(ConstraintViolationException ex, WebRequest request) {
        List<String> errors = new ArrayList<>();
        for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            errors.add(formatViolation(violation));
        }

        RestApiErrors restApiErrors =
                new RestApiErrors(
                        HttpStatus.UNPROCESSABLE_ENTITY,
                        "Error occurred while validating resource",
                        errors
                );
        return new ResponseEntity<>(restApiErrors, new HttpHeaders(), restApiErrors.getStatus());
    }

    @ExceptionHandler({ Exception.class })
    public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
        RestApiErrors restApiErrors = new RestApiErrors(
                HttpStatus.INTERNAL_SERVER_ERROR, ex.getLocalizedMessage(), "error occurred");
        return new ResponseEntity<>(restApiErrors, new HttpHeaders(), restApiErrors.getStatus());
    }

    private static String formatViolation(ConstraintViolation<?> violation){
        String argName = ((PathImpl) violation.getPropertyPath()).getLeafNode().getName();
        if(StringUtils.isEmpty(argName))
            return violation.getMessage();
        return String.format("%s: %s", argName, violation.getMessage());
    }
}
