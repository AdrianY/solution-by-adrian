package com.techasiaportal.cc.solution.adrian.commons.rest.infra.documentation;

public interface RestConfigReferences {
    String API_DOCUMENTATION_PROPERTIES_PREFIX = "sab.web.rest.documentation";
}
