package com.techasiaportal.cc.solution.adrian.commons.rest.api.support;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.http.HttpStatus;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collections;
import java.util.List;

@ApiModel(value = "RestApiErrors", description = "contains information why the operation failed")
@XmlRootElement(name = "RestApiErrors")
public class RestApiErrors {

    @ApiModelProperty(
            notes = "HTTP status code",
            required = true,
            readOnly = true
    )
    private final HttpStatus status;

    @ApiModelProperty(
            notes = "Error message",
            required = true,
            readOnly = true
    )
    private final String message;

    @ApiModelProperty(
            notes = "Specific errors. Useful for retrieving error message for 422 HTTP responses",
            readOnly = true
    )
    private final List<String> errors;

    public RestApiErrors(HttpStatus status, String message, List<String> errors) {
        this.status = status;
        this.message = message;
        this.errors = errors;
    }

    public RestApiErrors(HttpStatus status, String message, String error) {
        this.status = status;
        this.message = message;
        errors = Collections.singletonList(error);
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<String> getErrors() {
        return errors;
    }
}
