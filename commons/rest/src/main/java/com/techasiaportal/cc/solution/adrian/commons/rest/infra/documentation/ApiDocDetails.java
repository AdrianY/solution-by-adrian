package com.techasiaportal.cc.solution.adrian.commons.rest.infra.documentation;

public class ApiDocDetails {
    private final ContactDetails contactDetails = new ContactDetails();
    private String title;
    private String description;
    private String version;
    private String license = "MIT";
    private String licenseUrl = "https://opensource.org/licenses/MIT";

    public ContactDetails getContactDetails() {
        return contactDetails;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getVersion() {
        return version;
    }

    public String getLicense() {
        return license;
    }

    public String getLicenseUrl() {
        return licenseUrl;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public void setLicenseUrl(String licenseUrl) {
        this.licenseUrl = licenseUrl;
    }

    public static class ContactDetails{

        private String name = "Adrian Yago";
        private String url = "https://github.com/ayago";
        private String email = "adriancyago@gmail.com";

        public String getName() {
            return name;
        }

        public String getUrl() {
            return url;
        }

        public String getEmail() {
            return email;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}


