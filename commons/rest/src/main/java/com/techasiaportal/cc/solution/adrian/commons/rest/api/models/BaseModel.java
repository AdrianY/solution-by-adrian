package com.techasiaportal.cc.solution.adrian.commons.rest.api.models;

import io.swagger.annotations.ApiModelProperty;

public abstract class BaseModel {

    @ApiModelProperty(
            notes = "The database generated ID",
            required = true,
            readOnly = true
    )
    private Long id;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
