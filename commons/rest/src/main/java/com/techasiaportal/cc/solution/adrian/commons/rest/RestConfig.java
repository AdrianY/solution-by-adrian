package com.techasiaportal.cc.solution.adrian.commons.rest;

import com.techasiaportal.cc.solution.adrian.commons.rest.implementation.RestImplementation;
import com.techasiaportal.cc.solution.adrian.commons.rest.infra.documentation.DocumentationConfig;
import com.techasiaportal.cc.solution.adrian.commons.rest.infra.marshalling.MarshallingConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
        RestImplementation.class,
        DocumentationConfig.class,
        MarshallingConfig.class
})
public class RestConfig {
}
