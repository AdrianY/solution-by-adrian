package com.techasiaportal.cc.solution.adrian.commons.rest.api.endpoints;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.services.MapperService;
import com.techasiaportal.cc.solution.adrian.commons.persistence.api.services.MapperServiceAware;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import java.lang.reflect.ParameterizedType;
import java.util.Collections;
import java.util.List;

/**
 * Base type of Documented REST endpoints using Springfox annotations
 *
 * For RESTful webservices which are documented using annotations
 * a separate layer called Models are needed to be used. These types serve
 * two purposes: API documentation and Representation (e.g. JAXB annotations).
 */
public abstract class DocumentedMappedResource<MODEL, DTO> implements MapperServiceAware, InitializingBean {

    private Class<DTO> dtoClass;
    private Class<MODEL> modelClass;
    private MapperService mapperService;

    @Override
    public void setMapperService(MapperService mapperService) {
        this.mapperService = mapperService;
    }

    protected MODEL toModel(DTO dto){
        return mapperService.map(dto, modelClass);
    }

    protected DTO toDTO(MODEL model){
        return mapperService.map(model, dtoClass);
    }

    protected List<MODEL> toModel(List<DTO> dtoList){
        return CollectionUtils.isEmpty(dtoList) ?
                Collections.emptyList() :
                mapperService.map(dtoList, modelClass);
    }

    @SuppressWarnings("unchecked")
    private Class<DTO> getDTOType() {
        return (Class<DTO>) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[1];
    }

    @SuppressWarnings("unchecked")
    private Class<MODEL> getModelType() {
        return (Class<MODEL>) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
    }

    @Override
    public void afterPropertiesSet() {
        dtoClass = getDTOType();
        modelClass = getModelType();
    }
}
