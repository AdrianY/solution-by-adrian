package com.techasiaportal.cc.solution.adrian.commons.security.implementation.filters.impl;

import com.techasiaportal.cc.solution.adrian.commons.security.implementation.properties.JwtSecurityProperties;
import io.jsonwebtoken.Jwts;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

import static com.techasiaportal.cc.solution.adrian.commons.security.implementation.references.SecurityReferences.HEADER_STRING;
import static com.techasiaportal.cc.solution.adrian.commons.security.implementation.references.SecurityReferences.TOKEN_PREFIX;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private final JwtSecurityProperties jwtSecurityProperties;

    JWTAuthorizationFilter(
            AuthenticationManager authenticationManager, JwtSecurityProperties jwtSecurityProperties) {
        super(authenticationManager);
        this.jwtSecurityProperties = jwtSecurityProperties;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain
    ) throws IOException, ServletException {
        String header = request.getHeader(HEADER_STRING);

        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(request, response);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(request);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);

         if(StringUtils.isEmpty(token))
             return null;

         String username = Jwts.parser()
                .setSigningKey(jwtSecurityProperties.getSecret().getBytes())
                .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                .getBody()
                .getSubject();

        if(StringUtils.isEmpty(username))
            return null;

        return new UsernamePasswordAuthenticationToken(username, null, Collections.emptyList());
    }
}
