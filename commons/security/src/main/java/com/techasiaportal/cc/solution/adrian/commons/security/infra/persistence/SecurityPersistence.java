package com.techasiaportal.cc.solution.adrian.commons.security.infra.persistence;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import static com.techasiaportal.cc.solution.adrian.commons.security.infra.persistence.SecurityPersistenceReferences.JPA_ENTITIES_PACKAGES;
import static com.techasiaportal.cc.solution.adrian.commons.security.infra.persistence.SecurityPersistenceReferences.JPA_REPOSITORY_PACKAGES;

@Configuration
@EntityScan(basePackages = {JPA_ENTITIES_PACKAGES})
@EnableJpaRepositories(basePackages = JPA_REPOSITORY_PACKAGES)
public class SecurityPersistence {
}
