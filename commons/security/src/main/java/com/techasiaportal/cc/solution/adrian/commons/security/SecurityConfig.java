package com.techasiaportal.cc.solution.adrian.commons.security;

import com.techasiaportal.cc.solution.adrian.commons.security.implementation.SecurityImplementation;
import com.techasiaportal.cc.solution.adrian.commons.security.infra.mappings.SecurityMappings;
import com.techasiaportal.cc.solution.adrian.commons.security.infra.persistence.SecurityPersistence;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
        SecurityImplementation.class,
        SecurityMappings.class,
        SecurityPersistence.class
})
public class SecurityConfig {
}
