package com.techasiaportal.cc.solution.adrian.commons.security.implementation.services;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.services.ResourceMaintenanceService;
import com.techasiaportal.cc.solution.adrian.commons.security.implementation.dto.ApplicationUserDTO;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface ApplicationUserService extends ResourceMaintenanceService<ApplicationUserDTO>, UserDetailsService {
}
