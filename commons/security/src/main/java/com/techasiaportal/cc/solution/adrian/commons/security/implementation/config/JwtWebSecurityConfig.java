package com.techasiaportal.cc.solution.adrian.commons.security.implementation.config;

import com.techasiaportal.cc.solution.adrian.commons.security.implementation.filters.AuthenticationFilterFactory;
import com.techasiaportal.cc.solution.adrian.commons.security.implementation.filters.impl.JWTAuthenticationFilter;
import com.techasiaportal.cc.solution.adrian.commons.security.implementation.filters.impl.JWTAuthorizationFilter;
import com.techasiaportal.cc.solution.adrian.commons.security.implementation.services.ApplicationUserService;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class JwtWebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final AuthenticationFilterFactory<JWTAuthenticationFilter> jwtAuthenticationFilterFactory;
    private final AuthenticationFilterFactory<JWTAuthorizationFilter> jwtAuthorisationFilterFactory;
    private final ApplicationUserService applicationUserService;
    private final PasswordEncoder bCryptPasswordEncoder;

    public JwtWebSecurityConfig(
            AuthenticationFilterFactory<JWTAuthenticationFilter> jwtAuthenticationFilterFactory,
            AuthenticationFilterFactory<JWTAuthorizationFilter> jwtAuthorisationFilterFactory,
            ApplicationUserService applicationUserService,
            PasswordEncoder bCryptPasswordEncoder
    ) {
        this.jwtAuthenticationFilterFactory = jwtAuthenticationFilterFactory;
        this.jwtAuthorisationFilterFactory = jwtAuthorisationFilterFactory;
        this.applicationUserService = applicationUserService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().authorizeRequests()
                .antMatchers(
                        "/v2/api-docs",
                        "/configuration/ui",
                        "/swagger-resources",
                        "/configuration/security",
                        "/swagger-ui.html",
                        "/webjars/**",
                        "/swagger-resources/configuration/ui",
                        "/swagger-ui.html").permitAll() //a workaround, should have externalised at Docket side
                .anyRequest().authenticated()
                .and()
                .addFilter(jwtAuthenticationFilterFactory.create(authenticationManager()))
                .addFilter(jwtAuthorisationFilterFactory.create(authenticationManager()))
                // this disables session creation on Spring Security
                .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(applicationUserService).passwordEncoder(bCryptPasswordEncoder);
    }

}
