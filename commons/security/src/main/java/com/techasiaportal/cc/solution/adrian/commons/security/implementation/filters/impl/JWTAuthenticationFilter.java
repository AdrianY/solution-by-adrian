package com.techasiaportal.cc.solution.adrian.commons.security.implementation.filters.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.techasiaportal.cc.solution.adrian.commons.security.implementation.entities.ApplicationUser;
import com.techasiaportal.cc.solution.adrian.commons.security.implementation.properties.JwtSecurityProperties;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;

import static com.techasiaportal.cc.solution.adrian.commons.security.implementation.references.SecurityReferences.HEADER_STRING;
import static com.techasiaportal.cc.solution.adrian.commons.security.implementation.references.SecurityReferences.TOKEN_PREFIX;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final JwtSecurityProperties jwtSecurityProperties;
    private final ObjectMapper objectMapper;
    private final AuthenticationManager authenticationManager;

    JWTAuthenticationFilter(
            JwtSecurityProperties jwtSecurityProperties,
            ObjectMapper objectMapper,
            AuthenticationManager authenticationManager
    ) {
        this.jwtSecurityProperties = jwtSecurityProperties;
        this.objectMapper = objectMapper;
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(
            HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {

        try {
            ApplicationUser credentials = objectMapper
                    .readValue(request.getInputStream(), ApplicationUser.class);

            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            credentials.getUsername(),
                            credentials.getPassword(),
                            Collections.emptyList()
                    )
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain,
            Authentication authentication
    ) {

        String token = Jwts.builder()
                .setSubject(getSubject(authentication))
                .setExpiration(getExpirationDate())
                .signWith(SignatureAlgorithm.HS512, jwtSecurityProperties.getSecret().getBytes())
                .compact();

        response.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
    }

    private String getSubject(Authentication authentication) {
        return ((User) authentication.getPrincipal()).getUsername();
    }

    private Date getExpirationDate() {
        return new Date(System.currentTimeMillis() + jwtSecurityProperties.getExpirationTime());
    }
}
