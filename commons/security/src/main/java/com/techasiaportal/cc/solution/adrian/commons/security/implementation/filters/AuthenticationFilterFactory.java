package com.techasiaportal.cc.solution.adrian.commons.security.implementation.filters;

import org.springframework.security.authentication.AuthenticationManager;

public interface AuthenticationFilterFactory<FILTER> {
    FILTER create(AuthenticationManager authenticationManager);
}
