package com.techasiaportal.cc.solution.adrian.commons.security.implementation.dto;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.dto.BaseDTO;
import org.hibernate.validator.constraints.NotBlank;

public class ApplicationUserDTO extends BaseDTO {

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
