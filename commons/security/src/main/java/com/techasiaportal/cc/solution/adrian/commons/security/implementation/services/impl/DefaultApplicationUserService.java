package com.techasiaportal.cc.solution.adrian.commons.security.implementation.services.impl;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.services.DefaultResourceMaintenanceService;
import com.techasiaportal.cc.solution.adrian.commons.security.implementation.dao.jpa.ApplicationUserJpaDao;
import com.techasiaportal.cc.solution.adrian.commons.security.implementation.dto.ApplicationUserDTO;
import com.techasiaportal.cc.solution.adrian.commons.security.implementation.entities.ApplicationUser;
import com.techasiaportal.cc.solution.adrian.commons.security.implementation.services.ApplicationUserService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;

@Service
@Transactional
public class DefaultApplicationUserService
        extends DefaultResourceMaintenanceService<ApplicationUserDTO, ApplicationUser>
        implements ApplicationUserService {

    private final PasswordEncoder bCryptPasswordEncoder;
    private final ApplicationUserJpaDao applicationUserJpaDao;

    public DefaultApplicationUserService(
            PasswordEncoder bCryptPasswordEncoder, ApplicationUserJpaDao applicationUserJpaDao) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.applicationUserJpaDao = applicationUserJpaDao;
        setDao(applicationUserJpaDao);
    }

    @Override
    public ApplicationUserDTO create(ApplicationUserDTO resource) {
        resource.setPassword(bCryptPasswordEncoder.encode(resource.getPassword()));
        return super.create(resource);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ApplicationUser applicationUser = applicationUserJpaDao.findByUsername(username);
        if (applicationUser == null) {
            throw new UsernameNotFoundException(username);
        }
        return new User(applicationUser.getUsername(), applicationUser.getPassword(), Collections.emptyList());
    }
}
