package com.techasiaportal.cc.solution.adrian.commons.security.infra.mappings;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.infra.MapperFactoryAware;
import com.techasiaportal.cc.solution.adrian.commons.security.implementation.dto.ApplicationUserDTO;
import com.techasiaportal.cc.solution.adrian.commons.security.implementation.entities.ApplicationUser;
import ma.glasnost.orika.MapperFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SecurityMappings implements MapperFactoryAware {

    @Override
    public void setMapperFactory(MapperFactory mapperFactory) {
        mapperFactory.classMap(ApplicationUserDTO.class, ApplicationUser.class)
                .fieldBToA("id", "id")
                .byDefault()
                .register();
    }
}
