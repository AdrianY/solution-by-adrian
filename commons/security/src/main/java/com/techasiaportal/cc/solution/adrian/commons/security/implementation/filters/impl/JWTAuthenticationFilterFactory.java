package com.techasiaportal.cc.solution.adrian.commons.security.implementation.filters.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.techasiaportal.cc.solution.adrian.commons.security.implementation.filters.AuthenticationFilterFactory;
import com.techasiaportal.cc.solution.adrian.commons.security.implementation.properties.JwtSecurityProperties;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Component;

@Component
public class JWTAuthenticationFilterFactory implements AuthenticationFilterFactory<JWTAuthenticationFilter> {

    private final JwtSecurityProperties jwtSecurityProperties;
    private final ObjectMapper objectMapper;

    public JWTAuthenticationFilterFactory(JwtSecurityProperties jwtSecurityProperties, ObjectMapper objectMapper) {
        this.jwtSecurityProperties = jwtSecurityProperties;
        this.objectMapper = objectMapper;
    }

    @Override
    public JWTAuthenticationFilter create(AuthenticationManager authenticationManager) {
        return new JWTAuthenticationFilter(
                jwtSecurityProperties,
                objectMapper,
                authenticationManager
        );
    }
}
