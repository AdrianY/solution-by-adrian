package com.techasiaportal.cc.solution.adrian.commons.security.implementation.entities;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.entities.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class ApplicationUser extends BaseEntity {

    @Column
    private String username;

    @Column
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
