package com.techasiaportal.cc.solution.adrian.commons.security.infra.persistence;

public interface SecurityPersistenceReferences {
    String JPA_REPOSITORY_PACKAGES = "com.techasiaportal.cc.solution.adrian.commons.security.implementation.dao.jpa";
    String JPA_ENTITIES_PACKAGES = "com.techasiaportal.cc.solution.adrian.commons.security.implementation.entities";
}
