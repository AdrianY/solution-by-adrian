package com.techasiaportal.cc.solution.adrian.commons.security.implementation.dao.jpa;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.dao.jpa.BaseJpaDao;
import com.techasiaportal.cc.solution.adrian.commons.security.implementation.entities.ApplicationUser;

public interface ApplicationUserJpaDao extends BaseJpaDao<ApplicationUser> {
    ApplicationUser findByUsername(String username);
}
