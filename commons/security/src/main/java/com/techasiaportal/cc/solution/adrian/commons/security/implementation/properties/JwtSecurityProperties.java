package com.techasiaportal.cc.solution.adrian.commons.security.implementation.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import static com.techasiaportal.cc.solution.adrian.commons.security.implementation.references.SecurityReferences.SECURITY_JWT_CONFIG;


@ConfigurationProperties(prefix = SECURITY_JWT_CONFIG)
@Component
public class JwtSecurityProperties {

    private String secret;
    private Long expirationTime;

    public Long getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Long expirationTime) {
        this.expirationTime = expirationTime;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
