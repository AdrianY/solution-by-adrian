package com.techasiaportal.cc.solution.adrian.commons.security.implementation.filters.impl;

import com.techasiaportal.cc.solution.adrian.commons.security.implementation.filters.AuthenticationFilterFactory;
import com.techasiaportal.cc.solution.adrian.commons.security.implementation.properties.JwtSecurityProperties;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Component;

@Component
public class JWTAuthorizationFilterFactory implements AuthenticationFilterFactory<JWTAuthorizationFilter> {

    private final JwtSecurityProperties jwtSecurityProperties;

    public JWTAuthorizationFilterFactory(JwtSecurityProperties jwtSecurityProperties) {
        this.jwtSecurityProperties = jwtSecurityProperties;
    }

    public JWTAuthorizationFilter create(AuthenticationManager authenticationManager){
        return new JWTAuthorizationFilter(authenticationManager, jwtSecurityProperties);
    }
}
