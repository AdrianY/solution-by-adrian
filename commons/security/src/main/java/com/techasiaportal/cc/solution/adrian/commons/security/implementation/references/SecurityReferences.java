package com.techasiaportal.cc.solution.adrian.commons.security.implementation.references;

public interface SecurityReferences {
    String TOKEN_PREFIX = "Bearer ";
    String HEADER_STRING = "Authorization";
    String SECURITY_JWT_CONFIG = "sab.web.rest.security.jwt";
}
