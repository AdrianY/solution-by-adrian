package com.techasiaportal.cc.solution.adrian.dist;


import com.techasiaportal.cc.solution.adrian.dist.infra.SolutionByAdrianCustomConfig;
import com.techasiaportal.cc.solution.adrian.rest.SolutionRestModule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({
        SolutionRestModule.class,
        SolutionByAdrianCustomConfig.class
})
public class SolutionByAdrianApplication {
    public static void main(String[] args) {
        SpringApplication.run(SolutionByAdrianApplication.class, args);
    }
}
