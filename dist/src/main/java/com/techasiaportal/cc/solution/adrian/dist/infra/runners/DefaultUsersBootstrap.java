package com.techasiaportal.cc.solution.adrian.dist.infra.runners;

import com.techasiaportal.cc.solution.adrian.commons.security.implementation.dto.ApplicationUserDTO;
import com.techasiaportal.cc.solution.adrian.commons.security.implementation.services.ApplicationUserService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DefaultUsersBootstrap implements ApplicationRunner {

    private final ApplicationUserService applicationUserService;

    public DefaultUsersBootstrap(ApplicationUserService applicationUserService) {
        this.applicationUserService = applicationUserService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        applicationUserService.create(user("ayago","ayago"));
        applicationUserService.create(user("matias","matias"));
        applicationUserService.create(user("fidel","fidel"));
        applicationUserService.create(user("dave","dave"));
    }

    private ApplicationUserDTO user(String username, String password){
        ApplicationUserDTO applicationUser = new ApplicationUserDTO();
        applicationUser.setUsername(username);
        applicationUser.setPassword(password);
        return applicationUser;
    }
}
