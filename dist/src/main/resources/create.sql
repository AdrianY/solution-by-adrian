CREATE TABLE IF NOT EXISTS white_listed_ip (
  id INTEGER AUTO_INCREMENT,
  ip VARCHAR(15),
  environment VARCHAR(10),
  app VARCHAR(15),
  PRIMARY KEY(id),
  UNIQUE KEY(ip, environment, app)
);

CREATE TABLE IF NOT EXISTS environments (
  id   INTEGER AUTO_INCREMENT,
  name VARCHAR(10),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS applications (
  id   INTEGER AUTO_INCREMENT,
  name VARCHAR(15),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS users (
  id   INTEGER AUTO_INCREMENT,
  username VARCHAR(50),
  password VARCHAR(255),
  PRIMARY KEY (id)
);