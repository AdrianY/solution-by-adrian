Solution by Adrian (sab)
========================

This is ayago's solution for TechAsia Portal's coding challenge. The server context of
this solution is */sab*

# Prerequisite

1. You should have Maven 3 installed in the environment where you are going
to try this. To check execute <code>mvn -v</code>.

2. For less hassle, free up your port 8080.

# Running the app

1. Build the application by executing <code>mvn clean install</code> in
the root folder. This will create the target folders which contains the build files per each module.

2. Go to dist > target folder and run the application by using command
<code>java -jar solution-by-adrian.jar com.techasiaportal.cc.solution.adrian.dist.SolutionByAdrianApplication</code>.
You should be able to see the startup logs.

*Note: if you want to deploy this separately just extract the lib and solution-by-adrian.jar in dist > target folder*

# Using the app

## To access the webservice directly

1. Login using the local part of your email as username and password.
Send an HTTP Post as follows to this link *http://localhost:8080/sab/login*:


    Content-type: application/json
    Body: {"username":your username,"password":your username}

If you have successfully login, this endpoint will return a JWT in the Authorization header in the form of


    Bearer JWT
    e.g. Bearer gfgdfkdsgkfjgskfgskdgfkdsgkfdsgkfjdsk.fdsgfgdshfgdsfgdhgfsdfsdg.fdsfdsgfgdsfgdsjfgfg

2. Access the White Listed IPs via this endpoint *http://localhost:8080/sab/white-listed-ips*.
When performing a request always include the JWT in the header as

    
    Authorization: Bearer <JWT>

## Swagger Documentation

To view **How to use my webservices**: use this link *http://localhost:8080/sab/swagger-ui.html*. When performing a
request you should be authorized using the button *Authorize*. Copy the JWT Token in the authorization field so that
you can use the Swagger UI to test the webservices.

# Features

* RESTFul API
* Content Negotiation (supports JSON and XML)
* Validation support
* Exception to HTTP code translation
* API Security using [JWT](https://jwt.io/introduction/) Authentication
* Filtering using [RSql](https://github.com/jirutka/rsql-parser)
* Swagger API documentation (annotation based)
* Modular approach (Multi-module maven project)
* In memory database using H2