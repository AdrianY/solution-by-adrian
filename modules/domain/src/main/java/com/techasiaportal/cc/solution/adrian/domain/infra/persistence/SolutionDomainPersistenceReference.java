package com.techasiaportal.cc.solution.adrian.domain.infra.persistence;


public interface SolutionDomainPersistenceReference {
    String JPA_REPOSITORY_PACKAGES = "com.techasiaportal.cc.solution.adrian.domain.implementation.dao.jpa";
    String JPA_ENTITIES_PACKAGES = "com.techasiaportal.cc.solution.adrian.domain.implementation.entities";
}
