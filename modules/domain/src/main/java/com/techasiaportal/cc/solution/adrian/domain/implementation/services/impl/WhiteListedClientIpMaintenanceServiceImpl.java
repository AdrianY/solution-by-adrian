package com.techasiaportal.cc.solution.adrian.domain.implementation.services.impl;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.services.DefaultResourceMaintenanceService;
import com.techasiaportal.cc.solution.adrian.domain.implementation.dao.jpa.WhiteListedClientIpJpaDao;
import com.techasiaportal.cc.solution.adrian.domain.implementation.dto.WhiteListedClientIpDTO;
import com.techasiaportal.cc.solution.adrian.domain.implementation.entities.WhiteListedClientIp;
import com.techasiaportal.cc.solution.adrian.domain.implementation.services.WhiteListedClientIpMaintenanceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

@Service
@Transactional
@Validated
public class WhiteListedClientIpMaintenanceServiceImpl
        extends DefaultResourceMaintenanceService<WhiteListedClientIpDTO, WhiteListedClientIp>
        implements WhiteListedClientIpMaintenanceService{

    public WhiteListedClientIpMaintenanceServiceImpl(WhiteListedClientIpJpaDao whiteListedClientIpJpaDao) {
        setDao(whiteListedClientIpJpaDao);
    }
}
