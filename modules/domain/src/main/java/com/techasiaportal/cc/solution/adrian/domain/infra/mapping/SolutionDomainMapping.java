package com.techasiaportal.cc.solution.adrian.domain.infra.mapping;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.infra.MapperFactoryAware;
import com.techasiaportal.cc.solution.adrian.domain.implementation.dto.WhiteListedClientIpDTO;
import com.techasiaportal.cc.solution.adrian.domain.implementation.entities.WhiteListedClientIp;
import ma.glasnost.orika.MapperFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SolutionDomainMapping implements MapperFactoryAware {

    @Override
    public void setMapperFactory(MapperFactory mapperFactory) {
        mapperFactory.classMap(WhiteListedClientIpDTO.class, WhiteListedClientIp.class)
                .fieldBToA("id", "id")
                .byDefault()
                .register();
    }

}
