package com.techasiaportal.cc.solution.adrian.domain.implementation.dao.jpa;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.dao.jpa.BaseJpaDao;
import com.techasiaportal.cc.solution.adrian.domain.implementation.entities.WhiteListedClientIp;

public interface WhiteListedClientIpJpaDao extends BaseJpaDao<WhiteListedClientIp>{
    boolean existsByIpAndAppAndEnvironment(String ip, String app, String environment);
}