package com.techasiaportal.cc.solution.adrian.domain.implementation.dao.jpa;


import com.techasiaportal.cc.solution.adrian.commons.persistence.api.dao.jpa.BaseJpaDao;
import com.techasiaportal.cc.solution.adrian.domain.implementation.entities.RegisteredEnvironments;

public interface RegisteredEnvironmentsJpaDao extends BaseJpaDao<RegisteredEnvironments> {
    boolean existsByName(String name);
}
