package com.techasiaportal.cc.solution.adrian.domain.implementation.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UniqueClientIpValidator.class)
public @interface UniqueClientIp {
    String valueName() default "white listed ip";
    String message() default "Client ip already exists";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
