package com.techasiaportal.cc.solution.adrian.domain.implementation.validators;

import com.techasiaportal.cc.solution.adrian.commons.validation.api.validators.NonNullConstraintValidator;
import com.techasiaportal.cc.solution.adrian.domain.implementation.dao.jpa.RegisteredEnvironmentsJpaDao;
import org.springframework.stereotype.Component;

@Component
public class ExistingEnvironmentValidator extends NonNullConstraintValidator<ExistingEnvironment, String> {

    private final RegisteredEnvironmentsJpaDao registeredEnvironmentsJpaDao;

    public ExistingEnvironmentValidator(RegisteredEnvironmentsJpaDao registeredEnvironmentsJpaDao) {
        this.registeredEnvironmentsJpaDao = registeredEnvironmentsJpaDao;
    }

    @Override
    protected boolean doValidation(String value, ExistingEnvironment constraintAnnotation) {
        return registeredEnvironmentsJpaDao.existsByName(value);
    }

    @Override
    protected String getValueName(ExistingEnvironment constraintAnnotation) {
        return constraintAnnotation.valueName();
    }

    @Override
    protected String customErrorMessage(ExistingEnvironment constraintAnnotation) {
        return constraintAnnotation.message();
    }
}
