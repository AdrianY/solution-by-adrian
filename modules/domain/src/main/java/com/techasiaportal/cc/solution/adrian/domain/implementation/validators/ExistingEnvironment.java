package com.techasiaportal.cc.solution.adrian.domain.implementation.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ExistingEnvironmentValidator.class)
public @interface ExistingEnvironment {
    String valueName() default "environment";
    String message() default "Not a registered environment";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
