package com.techasiaportal.cc.solution.adrian.domain.implementation.dto;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.dto.BaseDTO;
import com.techasiaportal.cc.solution.adrian.commons.validation.api.validators.net.IpV4;
import com.techasiaportal.cc.solution.adrian.domain.implementation.validators.ExistingApplication;
import com.techasiaportal.cc.solution.adrian.domain.implementation.validators.ExistingEnvironment;
import com.techasiaportal.cc.solution.adrian.domain.implementation.validators.UniqueClientIp;


@UniqueClientIp
public class WhiteListedClientIpDTO extends BaseDTO {

    @IpV4
    private String ip;

    @ExistingEnvironment
    private String environment;

    @ExistingApplication
    private String app;

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getIp() {
        return ip;
    }

    public String getEnvironment() {
        return environment;
    }

    public String getApp() {
        return app;
    }
}
