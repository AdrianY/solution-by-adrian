package com.techasiaportal.cc.solution.adrian.domain.implementation.validators;

import com.techasiaportal.cc.solution.adrian.commons.validation.api.validators.NonNullConstraintValidator;
import com.techasiaportal.cc.solution.adrian.domain.implementation.dao.jpa.WhiteListedClientIpJpaDao;
import com.techasiaportal.cc.solution.adrian.domain.implementation.dto.WhiteListedClientIpDTO;
import org.springframework.stereotype.Component;

@Component
public class UniqueClientIpValidator extends NonNullConstraintValidator<UniqueClientIp, WhiteListedClientIpDTO> {

    private final WhiteListedClientIpJpaDao whiteListedClientIpJpaDao;

    public UniqueClientIpValidator(WhiteListedClientIpJpaDao whiteListedClientIpJpaDao) {
        this.whiteListedClientIpJpaDao = whiteListedClientIpJpaDao;
    }

    @Override
    protected boolean doValidation(WhiteListedClientIpDTO value, UniqueClientIp constraintAnnotation) {
        return !whiteListedClientIpJpaDao.existsByIpAndAppAndEnvironment(
                value.getIp(), value.getApp(), value.getEnvironment());
    }

    @Override
    protected String getValueName(UniqueClientIp constraintAnnotation) {
        return constraintAnnotation.valueName();
    }

    @Override
    protected String customErrorMessage(UniqueClientIp constraintAnnotation) {
        return constraintAnnotation.message();
    }
}
