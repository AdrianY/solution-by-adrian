package com.techasiaportal.cc.solution.adrian.domain.implementation.services;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.services.ResourceMaintenanceService;
import com.techasiaportal.cc.solution.adrian.domain.implementation.dto.WhiteListedClientIpDTO;

public interface WhiteListedClientIpMaintenanceService extends ResourceMaintenanceService<WhiteListedClientIpDTO> {

}
