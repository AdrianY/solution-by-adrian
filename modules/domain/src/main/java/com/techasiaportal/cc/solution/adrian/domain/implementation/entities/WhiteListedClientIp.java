package com.techasiaportal.cc.solution.adrian.domain.implementation.entities;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.entities.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "white_listed_ip")
public class WhiteListedClientIp extends BaseEntity {

    @Column
    private String ip;

    @Column
    private String environment;

    @Column
    private String app;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }


}
