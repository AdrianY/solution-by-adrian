package com.techasiaportal.cc.solution.adrian.domain.implementation.dao.jpa;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.dao.jpa.BaseJpaDao;
import com.techasiaportal.cc.solution.adrian.domain.implementation.entities.RegisteredApplications;

public interface RegisteredApplicationsJpaDao extends BaseJpaDao<RegisteredApplications> {
    boolean existsByName(String name);
}
