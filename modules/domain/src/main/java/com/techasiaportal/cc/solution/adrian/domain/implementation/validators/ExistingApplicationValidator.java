package com.techasiaportal.cc.solution.adrian.domain.implementation.validators;

import com.techasiaportal.cc.solution.adrian.commons.validation.api.validators.NonNullConstraintValidator;
import com.techasiaportal.cc.solution.adrian.domain.implementation.dao.jpa.RegisteredApplicationsJpaDao;
import org.springframework.stereotype.Component;

@Component
public class ExistingApplicationValidator extends NonNullConstraintValidator<ExistingApplication, String> {

    private final RegisteredApplicationsJpaDao registeredApplicationsJpaDao;

    public ExistingApplicationValidator(RegisteredApplicationsJpaDao registeredApplicationsJpaDao) {
        this.registeredApplicationsJpaDao = registeredApplicationsJpaDao;
    }

    @Override
    protected boolean doValidation(String value, ExistingApplication constraintAnnotation) {
        return registeredApplicationsJpaDao.existsByName(value);
    }

    @Override
    protected String getValueName(ExistingApplication constraintAnnotation) {
        return constraintAnnotation.valueName();
    }

    @Override
    protected String customErrorMessage(ExistingApplication constraintAnnotation) {
        return constraintAnnotation.message();
    }
}
