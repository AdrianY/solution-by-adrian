package com.techasiaportal.cc.solution.adrian.domain.implementation.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ExistingApplicationValidator.class)
public @interface ExistingApplication {
    String valueName() default "application";
    String message() default "Not a registered application";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
