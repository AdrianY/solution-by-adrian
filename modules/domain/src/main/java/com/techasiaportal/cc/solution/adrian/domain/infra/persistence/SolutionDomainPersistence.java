package com.techasiaportal.cc.solution.adrian.domain.infra.persistence;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import static com.techasiaportal.cc.solution.adrian.domain.infra.persistence.SolutionDomainPersistenceReference.JPA_ENTITIES_PACKAGES;
import static com.techasiaportal.cc.solution.adrian.domain.infra.persistence.SolutionDomainPersistenceReference.JPA_REPOSITORY_PACKAGES;

@Configuration
@EntityScan(basePackages = {JPA_ENTITIES_PACKAGES})
@EnableJpaRepositories(basePackages = JPA_REPOSITORY_PACKAGES)
public class SolutionDomainPersistence {
}
