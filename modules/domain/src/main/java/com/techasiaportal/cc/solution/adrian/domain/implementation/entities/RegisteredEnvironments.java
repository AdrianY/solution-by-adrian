package com.techasiaportal.cc.solution.adrian.domain.implementation.entities;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.entities.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "environments")
public class RegisteredEnvironments extends BaseEntity {

    @Column
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
