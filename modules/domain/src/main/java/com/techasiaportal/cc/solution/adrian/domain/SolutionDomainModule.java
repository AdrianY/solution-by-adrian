package com.techasiaportal.cc.solution.adrian.domain;

import com.techasiaportal.cc.solution.adrian.domain.implementation.SolutionDomainImplementation;
import com.techasiaportal.cc.solution.adrian.domain.infra.mapping.SolutionDomainMapping;
import com.techasiaportal.cc.solution.adrian.domain.infra.persistence.SolutionDomainPersistence;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Import;

@SpringBootConfiguration
@Import({
        SolutionDomainImplementation.class,
        SolutionDomainPersistence.class,
        SolutionDomainMapping.class
})
public class SolutionDomainModule {
}
