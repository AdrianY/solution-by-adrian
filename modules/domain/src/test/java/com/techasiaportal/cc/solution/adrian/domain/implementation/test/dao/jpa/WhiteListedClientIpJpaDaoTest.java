package com.techasiaportal.cc.solution.adrian.domain.implementation.test.dao.jpa;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.techasiaportal.cc.solution.adrian.commons.test.annotation.DaoTestConfiguration;
import com.techasiaportal.cc.solution.adrian.domain.implementation.dao.jpa.WhiteListedClientIpJpaDao;
import com.techasiaportal.cc.solution.adrian.domain.implementation.entities.WhiteListedClientIp;
import com.techasiaportal.cc.solution.adrian.domain.infra.persistence.SolutionDomainPersistence;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@DaoTestConfiguration
@ContextConfiguration(classes = SolutionDomainPersistence.class)
class WhiteListedClientIpJpaDaoTest {

    @Autowired
    private WhiteListedClientIpJpaDao whiteListedClientIpJpaDao;

    @Test
    @DisplayName("Jpa repository whiteListedClientIpJpaDao should be able to retrieve data")
    @DatabaseSetup("/dao/WhiteListedClientIpJpaDaoTest/test1_setup.xml")
    void test1(){
        List<WhiteListedClientIp> allIps = whiteListedClientIpJpaDao.findAll();
        assertTrue(CollectionUtils.isNotEmpty(allIps));
        assertEquals(1, allIps.size());
    }
}
