package com.techasiaportal.cc.solution.adrian.rest.implementation.endpoints;

import com.techasiaportal.cc.solution.adrian.commons.rest.api.endpoints.DocumentedMappedResource;
import com.techasiaportal.cc.solution.adrian.commons.rest.api.support.RestApiErrors;
import com.techasiaportal.cc.solution.adrian.domain.implementation.dto.WhiteListedClientIpDTO;
import com.techasiaportal.cc.solution.adrian.domain.implementation.services.WhiteListedClientIpMaintenanceService;
import com.techasiaportal.cc.solution.adrian.rest.implementation.models.WhiteListedClientIpModel;
import com.techasiaportal.cc.solution.adrian.rest.implementation.models.WhiteListedClientIpsModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(
        value = "/white-listed-ips",
        description = "Operations pertaining to maintaining White listed client IPs to our environments"
)
@RestController
@RequestMapping(value = "white-listed-ips/v1.0")
public class WhiteListedClientResource
        extends DocumentedMappedResource<WhiteListedClientIpModel, WhiteListedClientIpDTO> {

    private final WhiteListedClientIpMaintenanceService whiteListedClientIpMaintenanceService;

    public WhiteListedClientResource(
            WhiteListedClientIpMaintenanceService whiteListedClientIpMaintenanceService) {
        this.whiteListedClientIpMaintenanceService = whiteListedClientIpMaintenanceService;
    }

    @ApiOperation(value = "Retrieve all white listed client IPs")
    @ApiResponses(value = {
            @ApiResponse(
                    code = 200, message = "Successfully retrieved list", response = WhiteListedClientIpsModel.class),
    })
    @GetMapping(
            produces = {
                    MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE
            }
    )
    public ResponseEntity<WhiteListedClientIpsModel> search(
            @RequestParam(value = "search", required = false) String searchString) {

        List<WhiteListedClientIpDTO> listedClientIps = whiteListedClientIpMaintenanceService.findAll(searchString);

        WhiteListedClientIpsModel whiteListedClientIpsModel = new WhiteListedClientIpsModel();
        whiteListedClientIpsModel.setWhiteListedClientIps(toModel(listedClientIps));
        return ResponseEntity.ok(whiteListedClientIpsModel);
    }

    @ApiOperation(value = "White list client IP")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully white listed IP"),
            @ApiResponse(
                    code = 422,
                    message = "Error occurred while validating resource",
                    response = RestApiErrors.class
            )
    })
    @PostMapping(
            produces = {
                    MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE
            },
            consumes = {
                    MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE
            }
    )
    public ResponseEntity<WhiteListedClientIpModel> create(@RequestBody WhiteListedClientIpModel whiteListedClientIp){
        WhiteListedClientIpDTO addedIp =
                whiteListedClientIpMaintenanceService.create(toDTO(whiteListedClientIp));
        return new ResponseEntity<>(
                toModel(addedIp),
                HttpStatus.CREATED
        );
    }

    @ApiOperation(value = "Remove client IP from white list")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Successfully removed IP in the white list"),
            @ApiResponse(code = 404, message = "Error retrieving resource record", response = RestApiErrors.class)
    })
    @DeleteMapping(
            path = "/{id}",
            produces = {
                    MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE
            }
    )
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity remove(@PathVariable("id") Long id){
        whiteListedClientIpMaintenanceService.remove(id);
        return ResponseEntity.noContent().build();
    }

}
