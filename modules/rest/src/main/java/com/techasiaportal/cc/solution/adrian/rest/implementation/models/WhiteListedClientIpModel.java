package com.techasiaportal.cc.solution.adrian.rest.implementation.models;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.techasiaportal.cc.solution.adrian.commons.rest.api.models.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(
        value = "WhiteListedClientIp",
        description = "A white listed client IP mapped to an app deployed to specified environment"
)
@JacksonXmlRootElement(localName = "whiteListedClientIp")
public class WhiteListedClientIpModel extends BaseModel {

    @ApiModelProperty(
            notes = "IPv4 of client connecting to our applications",
            required = true
    )
    private String ip;

    @ApiModelProperty(
            notes = "Environment where the application being accessed by the client is deployed. " +
            "Should select only those registered in the system",
            required = true
    )
    private String environment;

    @ApiModelProperty(
            notes = "Name of the application being accessed by the client. " +
            "Should select only those registered in the system",
            required = true
    )
    private String app;

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getIp() {
        return ip;
    }

    public String getEnvironment() {
        return environment;
    }

    public String getApp() {
        return app;
    }
}
