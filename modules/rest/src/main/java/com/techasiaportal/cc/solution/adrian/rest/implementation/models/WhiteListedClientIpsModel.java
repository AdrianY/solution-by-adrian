package com.techasiaportal.cc.solution.adrian.rest.implementation.models;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value = "WhiteListedClientIps", description = "wrapper containing list of all white listed client IPs")
@JacksonXmlRootElement(localName = "whiteListedClientIps")
public class WhiteListedClientIpsModel {

    @ApiModelProperty(
            notes = "List of white listed client ips",
            required = true,
            readOnly = true
    )
    @JacksonXmlProperty(localName = "whiteListedClientIp")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<WhiteListedClientIpModel> whiteListedClientIps;

    public List<WhiteListedClientIpModel> getWhiteListedClientIps() {
        return whiteListedClientIps;
    }

    public void setWhiteListedClientIps(List<WhiteListedClientIpModel> whiteListedClientIps) {
        this.whiteListedClientIps = whiteListedClientIps;
    }

}
