package com.techasiaportal.cc.solution.adrian.rest;

import com.techasiaportal.cc.solution.adrian.domain.SolutionDomainModule;
import com.techasiaportal.cc.solution.adrian.rest.implementation.SolutionRestImplementation;
import com.techasiaportal.cc.solution.adrian.rest.infra.SolutionRestMapping;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Import;

@SpringBootConfiguration
@Import({
        SolutionDomainModule.class,
        SolutionRestImplementation.class,
        SolutionRestMapping.class
})
public class SolutionRestModule {
}
