package com.techasiaportal.cc.solution.adrian.rest.infra;

import com.techasiaportal.cc.solution.adrian.commons.persistence.api.infra.MapperFactoryAware;
import com.techasiaportal.cc.solution.adrian.domain.implementation.dto.WhiteListedClientIpDTO;
import com.techasiaportal.cc.solution.adrian.rest.implementation.models.WhiteListedClientIpModel;
import ma.glasnost.orika.MapperFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SolutionRestMapping implements MapperFactoryAware {
    @Override
    public void setMapperFactory(MapperFactory mapperFactory) {
        mapperFactory.classMap(WhiteListedClientIpModel.class, WhiteListedClientIpDTO.class)
                .fieldBToA("id", "id")
                .byDefault()
                .register();
    }
}
